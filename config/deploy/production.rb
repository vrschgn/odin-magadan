# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w(bma@212.15.114.234)
role :web, %w(bma@212.15.114.234)
role :db,  %w(bma@212.15.114.234)

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '212.15.114.234', user: 'bma', roles: %w(web app db),
                              ssh_options: {
                                forward_agent: true,
                                auth_methods: %w(publickey),
                                port: 5100
                              }

set :tmp_dir, '/home/bma/tmp'
set :deploy_to, '/home/bma/www/bma.rdms.ru'
set :branch, 'master'
set :rails_env, 'production'
set :environment, 'production'

set :application, 'bma'
set :rvm_type, :user 