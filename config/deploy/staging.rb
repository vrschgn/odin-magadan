# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w(rdms@goldengate.o-din.ru)
role :web, %w(rdms@goldengate.o-din.ru)
role :db,  %w(rdms@goldengate.o-din.ru)

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server 'goldengate.o-din.ru', user: 'rdms', roles: %w(web app db),
                              ssh_options: {
                                forward_agent: true,
                                auth_methods: %w(publickey),
                                port: 2250
                              }

set :tmp_dir, '/home/rdms/tmp'
set :deploy_to, '/home/rdms/www/'
set :branch, ENV['REVISION'] || 'develop'
set :rails_env, 'production'
set :environment, 'production'

set :application, 'rdms'
