# encoding: utf-8

##
# Backup Generated: goldengate_backup
# Once configured, you can run the backup with the following command:
#
# $ backup perform -t goldengate_backup [-c <path_to_configuration_file>]
#
# For more information about Backup's components, see the documentation at:
# http://backup.github.io/backup
#
Model.new(:app_backup, 'Description for backup') do
    ##
    # PostgreSQL [Database]
    #
    database PostgreSQL do |db|
      db.name               = ENV['DB_NAME']
      db.username           = ENV['DB_USERNAME']
      db.password           = ENV['DB_PASSWORD']
      db.host               = 'postgres'
      db.port               = 5432
    end
    
    ##
    # FTP (File Transfer Protocol) [Storage]
    #
    store_with FTP do |server|
      server.username     = 'reserve_do2'
      server.password     = 'XCtb9bE1'
      server.ip           = '5.178.87.183'
      server.port         = 21
      server.path         = '~/'
      server.keep         = 2
      server.passive_mode = false
    end
  
    store_with Local do |local|
      local.path       = "~/backups/"
      local.keep       = 5
    end
  
    ##
    # Gzip [Compressor]
    #
    compress_with Gzip
  
    ##
    # Mail [Notifier]
    #
    # The default delivery method for Mail Notifiers is 'SMTP'.
    # See the documentation for other delivery options.
    #
    notify_by Mail do |mail|
      mail.on_success           = true
      mail.on_warning           = true
      mail.on_failure           = true
  
      mail.from                 = 'backup@o-din.ru'
      mail.to                   = 'support@o-din.ru'
      mail.address              = 'smtp.yandex.ru'
      mail.port                 = 465
      mail.domain               = 'o-din.ru'
      mail.user_name            = 'support@o-din.ru'
      mail.password             = 'xmypoeytpo'
      mail.authentication       = 'plain'
      mail.encryption           = :ssl
    end
  
    notify_by Slack do |slack|
      slack.on_success = true
      slack.on_warning = true
      slack.on_failure = true
  
      # The integration token
      slack.webhook_url = 'https://hooks.slack.com/services/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'

      slack.icon_emoji = ':city_sunrise:'
    end
  end
  