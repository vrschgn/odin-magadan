#!/bin/sh
su app -c "cd /home/app/www && bundle &&
            bundle exec rake db:migrate RAILS_ENV=production && 
            bundle exec rake db:seed RAILS_ENV=production"

chown -R app:app /home/app/www/log
chown    app:app /home/app/www/public/system
chown    app:app /home/app/backups