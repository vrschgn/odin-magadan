job_type :runner, "cd /home/bma/www/bma.rdms.ru/current && bundle exec rails runner -e :environment ':task' :output"
job_type :ruby_command, '/usr/local/rvm/wrappers/ruby-2.3.1@rdms/:task >> Backup/log/whenever.log 2>&1'
job_type :logrotate_command, ':task >> logrotate/whenever.log 2>&1'

env :PATH, ENV['PATH']
set :output, 'log/whenever.log'

every :monday, at: '06:00' do
  runner 'PprIssueScheduler.new.assign_current_week'
end

every 1.day, at: '00:10' do
  ruby_command 'backup perform -t rdms_backup'
end

every 1.day, at: '00:20' do
  logrotate_command 'logrotate -v -s /home/rdms/logrotate.state /home/rdms/logrotate.conf'
end
