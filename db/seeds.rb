# coding: utf-8
#
# DO NOT MESS IN THIS FILE!!
# Seeds are running every deploy!
#

Rails.configuration.action_mailer.raise_delivery_errors = false

def create_object(klass, params)
  obj = klass.new(params)
  message = if obj.save
              "#{klass.name.humanize} #{params.inspect} was created"
            else
              "Can't create #{klass.name.humanize.downcase} #{params.inspect}"
            end

  puts message
  obj
end

facility_title = 'Test facility'
facility = Facility.find_by_title(facility_title) || create_object(Facility, title: facility_title)

company_name = 'Test company'
company = Company.find_by_name(company_name) || create_object(Company, name: company_name, facility: facility)

users = [{ email: 'superadmin@test', role: 'superadmin' },
         { email: 'admin@test', role: 'admin' },
         { email: 'user@test', role: 'user' },
         { email: 'dispatcher@test', role: 'dispatcher' },
         { email: 'executor@test', role: 'executor' }]
users.map! do |params|
  params.merge(company_id: company.id,
               password: '0000',
               facility_id: facility.id,
               approved: true,
               confirmed_at: Time.zone.now,
               fname: params[:role].capitalize,
               lname: 'Test')
end

users.each do |params|
  next if User.find_by_email(params[:email])
  create_object(User, params)
end

cleaning = 'cleaning'
engineering = 'engineering'
security = 'security'
administration = 'administration'

services = [{ title: 'Клининг', code: cleaning },
            { title: 'Инженерия', code: engineering },
            { title: 'Безопасность', code: security },
            { title: 'Администрация', code: administration }]

services.each do |params|
  next if Service.find_by(code: params[:code])
  create_object(Service, params)
end

cleaning_service = Service.find_by(code: cleaning)
engineering_service = Service.find_by(code: engineering)
security_service = Service.find_by(code: security)
administration_service = Service.find_by(code: administration)


cleaning_work_categories = ['Уборка мест общего пользования ',
                            'Уборка территории арендаторов',
                            'Внешняя уборка',
                            'Озеленение',
                            'Вывоз снега,ТБО и прочих отходов',
                            'Дератизация, дезинсекция, дезинфекция',
                            'Закупка расходных материалов ',
                            'Спецработы',
                            'Погрузка/Разгрузка',
                            'Гарантийная заявка']

engineering_work_categories = ['КН канализация',
                               'Кз канализация (засоры)',
                               'Ка неисправность арматуры унитазов',
                               'КДЦ кондиционирование',
                               'КДЦп протечки фенкойлов',
                               'В водоснабжение',
                               'ОВ общеобменная вентиляция',
                               'ОТ отопление',
                               'С Слаботочные системы (телефония, интернет, видеонаблюдение, СКУД, ТВ)',
                               'ЭЛ Электроснабжение',
                               'ОХР Обще-хозяйственные ремонты и работы',
                               'НЗ Неприятный запах',
                               'ПБ Пожарная безопасность',
                               'ЖХ Заявка по поводу "жарко-холодно"',
                               'ТС Неисправности в системе теплоснабжения',
                               'ДС Неисправности в дренажной системе',
                               'О Неисправности оконных блоков (фурнитура, трещины стеклопакетах, примыкание рам)',
                               'СТР Неисправности в строительной части здания',
                               'П Сопровождение работ, производимых подрядными организациями',
                               'Гарантийная заявка']
security_work_categories = []
administration_work_categories = ['Заявка на пропуск для человека',
                                  'Заявка на пропуск для автомобиля',
                                  'Заявка на допуск подрядчика (работы)',
                                  'Заявка на доставку',
                                  'Заявка на коммерческое предложение',
                                  'Заявка по безопасности',
                                  'Общая заявка']

work_categories = cleaning_work_categories.map       { |e| { title: e, service_id: cleaning_service.id } } +
                  engineering_work_categories.map    { |e| { title: e, service_id: engineering_service.id } } +
                  security_work_categories.map       { |e| { title: e, service_id: security_service.id } } +
                  administration_work_categories.map { |e| { title: e, service_id: administration_service.id } }

work_categories.each do |params|
  next if WorkCategory.find_by(params)
  create_object(WorkCategory, params)
end
