class AddUrgencyColumnsToIssues < ActiveRecord::Migration
  def change
  	add_column :issues, :urgency, :integer, default: 0, index: true
  end
end
