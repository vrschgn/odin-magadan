# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170408142350) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "assets", force: :cascade do |t|
    t.string   "type"
    t.integer  "assetable_id"
    t.string   "assetable_type"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "buildings", force: :cascade do |t|
    t.string   "title"
    t.string   "address"
    t.integer  "facility_id"
    t.string   "latitude"
    t.string   "longitude"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text     "text"
    t.string   "action"
    t.integer  "issue_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["issue_id"], name: "index_comments_on_issue_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "facility_id"
    t.boolean  "see_all_rooms",           default: false
    t.string   "short_name"
    t.string   "ktype"
    t.string   "is_person"
    t.string   "inn"
    t.string   "kpp"
    t.string   "ogrn"
    t.string   "okpo_code"
    t.string   "type_of_activity"
    t.text     "comment"
    t.string   "legal_address"
    t.string   "post_address"
    t.string   "phone"
    t.string   "fax"
    t.string   "head_position"
    t.string   "head"
    t.string   "contact_person_position"
    t.string   "contact_person"
    t.string   "contact_person_phone"
    t.string   "email"
    t.string   "form"
    t.text     "blocked_fields",          default: [],                 array: true
    t.string   "megaplan_id",             default: ""
  end

  add_index "companies", ["facility_id"], name: "index_companies_on_facility_id", using: :btree

  create_table "consumptions", force: :cascade do |t|
    t.integer  "material_id"
    t.integer  "issue_id"
    t.integer  "count"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "title"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "facility_id"
  end

  create_table "facilities", force: :cascade do |t|
    t.string   "title"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "code"
  end

  create_table "floors", force: :cascade do |t|
    t.string   "title"
    t.integer  "facility_id"
    t.string   "position"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "building_id"
  end

  add_index "floors", ["facility_id"], name: "index_floors_on_facility_id", using: :btree

  create_table "issues", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "responsible_full_name"
    t.string   "tel"
    t.string   "work_place"
    t.text     "description"
    t.string   "state"
    t.date     "dead_line"
    t.hstore   "settings"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "room_id"
    t.integer  "service_id"
    t.integer  "work_category_id"
    t.integer  "company_id"
    t.string   "type"
    t.date     "finish_date_plane"
    t.integer  "executor_id"
    t.integer  "urgency",               default: 0
    t.datetime "finished_at"
    t.string   "digest"
    t.datetime "deleted_at"
  end

  add_index "issues", ["deleted_at"], name: "index_issues_on_deleted_at", using: :btree
  add_index "issues", ["type"], name: "index_issues_on_type", using: :btree

  create_table "ku_equipment", force: :cascade do |t|
    t.string   "number"
    t.string   "title"
    t.integer  "room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "materials", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "facility_id"
    t.boolean  "hot"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "published"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "ppr_equipments", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "ppr_system_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "room_id"
    t.text     "technological_map"
  end

  add_index "ppr_equipments", ["ppr_system_id"], name: "index_ppr_equipments_on_ppr_system_id", using: :btree

  create_table "ppr_frequency_works", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.text     "template_weeks"
    t.string   "color"
    t.integer  "facility_id"
  end

  create_table "ppr_group_systems", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "company_id"
  end

  create_table "ppr_systems", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "ppr_group_system_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "ppr_systems", ["ppr_group_system_id"], name: "index_ppr_systems_on_ppr_group_system_id", using: :btree

  create_table "ppr_work_equipments", force: :cascade do |t|
    t.integer "equipment_id",                null: false
    t.integer "work_id",                     null: false
    t.boolean "is_active",    default: true
    t.integer "weeks",        default: [],                array: true
  end

  add_index "ppr_work_equipments", ["equipment_id"], name: "index_ppr_work_equipments_on_equipment_id", using: :btree
  add_index "ppr_work_equipments", ["work_id"], name: "index_ppr_work_equipments_on_work_id", using: :btree

  create_table "ppr_works", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "icon"
    t.integer  "ppr_system_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "ppr_frequency_work_id"
    t.string   "normo_hour"
  end

  add_index "ppr_works", ["ppr_system_id"], name: "index_ppr_works_on_ppr_system_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "title"
    t.integer  "floor_id"
    t.string   "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  add_index "rooms", ["floor_id"], name: "index_rooms_on_floor_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "type"
    t.integer  "sort_index", default: 0
    t.integer  "bs_class"
    t.string   "icon_class"
    t.string   "code"
    t.string   "css_class"
  end

  add_index "services", ["sort_index"], name: "index_services_on_sort_index", using: :btree
  add_index "services", ["type"], name: "index_services_on_type", using: :btree

  create_table "stock_categories", force: :cascade do |t|
    t.string   "title"
    t.string   "code"
    t.text     "description"
    t.integer  "stock_unit_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.integer  "children_count"
  end

  add_index "stock_categories", ["stock_unit_id"], name: "index_stock_categories_on_stock_unit_id", using: :btree

  create_table "stock_units", force: :cascade do |t|
    t.string   "title"
    t.string   "code"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "active"
  end

  create_table "user_facilities", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "facility_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "user_facilities", ["facility_id"], name: "index_user_facilities_on_facility_id", using: :btree
  add_index "user_facilities", ["user_id"], name: "index_user_facilities_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
    t.string   "fname"
    t.string   "mname"
    t.string   "lname"
    t.string   "tel"
    t.integer  "company_id"
    t.hstore   "settings"
    t.integer  "facility_id"
    t.integer  "service_id"
    t.string   "type"
    t.boolean  "sms_issue",              default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean  "approved",               default: false, null: false
    t.boolean  "email_issue",            default: false, null: false
    t.string   "authentication_token"
  end

  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "work_categories", force: :cascade do |t|
    t.string   "title"
    t.integer  "service_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
    t.string   "code"
  end

  add_index "work_categories", ["service_id"], name: "index_work_categories_on_service_id", using: :btree

  add_foreign_key "companies", "facilities"
  add_foreign_key "posts", "users"
  add_foreign_key "ppr_equipments", "ppr_systems"
  add_foreign_key "ppr_group_systems", "companies"
  add_foreign_key "ppr_systems", "ppr_group_systems"
  add_foreign_key "ppr_works", "ppr_frequency_works"
  add_foreign_key "ppr_works", "ppr_systems"
  add_foreign_key "user_facilities", "facilities"
  add_foreign_key "user_facilities", "users"
  add_foreign_key "users", "services"
  add_foreign_key "users", "services"
end
