class UserFacility < ActiveRecord::Base
  belongs_to :user
  belongs_to :available_facility, class_name: "Facility", foreign_key: "facility_id"
end
