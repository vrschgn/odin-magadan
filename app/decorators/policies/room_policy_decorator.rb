RoomPolicy.class_eval do
  RoomPolicy::Scope.class_eval do
    def resolve
      if user.admin? || user.superadmin?
        scope
      elsif user.multi_director?
        scope.by_available_facilities(user.available_facilities)
      elsif !user.user? || user.see_all_rooms?
        scope.by_facility(user.facility)
      else
        scope.by_company(user.company)
      end
    end

   
  end
end