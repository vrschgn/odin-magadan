UserPolicy.class_eval do
  UserPolicy::Scope.class_eval do
    def resolve
      if role_in? %w(admin superadmin)
        scope
      elsif role_in? %w(clerk office_manager owner)
        scope.by_facility(user.facility) 
      elsif role_in? %w(multi_director)
        scope.where(facility_id: user.available_facilities) 
      elsif user.chief_engineer?
        scope.with_role(:dispatcher, :executor, :user, :clerk, :office_manager)
      elsif user.company_admin?
        scope.by_company(user.company_id)
      else
        scope.none
      end
    end

   
  end
end