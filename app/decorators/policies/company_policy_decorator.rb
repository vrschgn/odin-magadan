CompanyPolicy.class_eval do
  CompanyPolicy::Scope.class_eval do

    def resolve
      if role_in? %w(admin superadmin)
        scope
      elsif role_in? %w(chief_engineer clerk director dispatcher service_admin owner)
        scope.where(facility_id: user.facility_id)
      elsif role_in? %w(multi_director)
        scope.where(facility_id: user.available_facilities) 
      else
        scope.where(id: user.company_id)
      end
    end
  end
end