FacilityPolicy.class_eval do
  FacilityPolicy::Scope.class_eval do
    def resolve
      if role_in? %w(admin superadmin )
        scope
      elsif role_in? %w(multi_director)
        scope.where(id: user.available_facilities) 
      else
        scope.where(id: user.facility.id)
      end
    end
  end
end