MaintenanceIssuePolicy.class_eval do
  MaintenanceIssuePolicy::Scope.class_eval do


    def resolve
      if role_in? %w(admin superadmin)
        scope.all
      elsif user.multi_director? 
        scope.by_available_facilities(user.available_facilities)
      elsif (role_in? %w(admin chief_engineer clerk director dispatcher  owner)) || user.see_all_rooms?
        scope.by_facility(user.facility)

      elsif (role_in? %w(executor service_admin office_manager)) || user.see_all_rooms?
        scope.by_facility(user.facility).by_service(user.service)
        # TODO: Set service for executor

      elsif (role_in? %w(user company_admin))
        scope.by_company(user.company)
     
      else
        scope.none
      end
    end



  end
end