UsersController.class_eval do

  def user_params
    params.require(:user).permit(:fname, :mname, :lname, :email,
                                 :role, :tel, :password, :password_confirmation,
                                 :company_id, :facility_id, :service_id,
                                 :sms_issue, :email_issue,  available_facility_ids: [])
  end
end