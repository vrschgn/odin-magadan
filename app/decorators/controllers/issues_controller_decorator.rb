IssuesController.class_eval do
  alias_method :engine_sub_issue_list_params, :sub_issue_list_params
  def sub_issue_list_params
    engine_sub_issue_list_params + [:urgency]
  end
end
