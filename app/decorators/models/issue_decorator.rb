Issue.class_eval do

  scope :by_available_facilities, -> (facilities)  { joins(:company).where(companies: { facility_id: facilities.pluck(:id).to_a }) }

  URGENCY_SCOPE = 'activerecord.attributes.issue.urgency'.freeze

  enum urgency: { normal: 0, urgent: 1, emergency: 2 }

  alias_method :engine_serializable_hash, :serializable_hash
  def serializable_hash(options = {})
    engine_serializable_hash(options)
      .reject { |key, _| %i(is_urgency is_urgency_label).include?(key) }
      .merge urgency:       urgency,
             urgency_label: I18n.t(urgency, scope: URGENCY_SCOPE)
  end

  def self.urgency_select
    urgencies.map { |key, _| [I18n.t("activerecord.attributes.issue.urgency_with_time.#{key}"), key] }
  end
end
