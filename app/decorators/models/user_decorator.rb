User.class_eval do

  has_many :available_facilities, through: :user_facilities
  has_many :user_facilities

  def delivery_info
    { id: id, name: full_name, phone: tel, email: email, role: role, company_name: company_name }
  end
end
