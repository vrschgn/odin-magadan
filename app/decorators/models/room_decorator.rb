Room.class_eval do
  
  scope :by_available_facilities, -> (facilities) do
    joins(:floor).where(floors: { facility_id: facilities.ids })
  end

end
