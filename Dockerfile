FROM phusion/passenger-ruby23

# Set correct environment variables.
ENV HOME /root

# Use baseimage-docker's init process.
CMD ["/sbin/my_init"]

#RUN apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold"
#RUN add-apt-repository universe
#RUN add-apt-repository ppa:certbot/certbot
RUN apt-get update

#certbot вынесен в отдельный сервис, неактуально
#RUN apt-get install -yq certbot \
#                        openssh-client \
#                        python-certbot-nginx \
#                        imagemagick \
#                        software-properties-common

                
RUN gem install backup

RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default

RUN mkdir /home/app/www

RUN mkdir /home/app/bundle
 
RUN rm -f /etc/service/redis/down
RUN rm -f /etc/service/memcached/down
RUN rm -f /etc/service/sshd/down

RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

USER app
ENV HOME /home/app
WORKDIR /home/app/www

ADD --chown=app:app config/docker/backup /home/app/backup

ADD --chown=app:app Gemfile /home/app/www
ADD --chown=app:app Gemfile.lock /home/app/www
ADD --chown=app:app bin /home/app/www/bin

ENV BUNDLER_HOME /home/app/www

#must be passed with args for build
ENV GIT_LOGIN_AUTH TRUE   
ENV GIT_LOGIN      docker_bot
ENV GIT_PASSWORD   uq5rkzr0di8wbs6zt0p7 

RUN set -uex; \ 
    bundle install;

# This will now install anything in Gemfile.tip
# This way you can add new gems without rebuilding _everything_ to add 1 gem
# Anything that was already installed from the main Gemfile will be re-used
ADD --chown=app:app Gemfile.tip /home/app/www

# Add rake and its dependencies
ADD --chown=app:app . /home/app/www

USER root

RUN mkdir /home/app/www/log
RUN mkdir /home/app/www/tmp

RUN touch /home/app/www/log/production.log

ADD config/docker/nginx/app.conf /etc/nginx/sites-enabled

# check recursive chown for www
RUN chown app:app /home/app/www/log
RUN chown app:app /home/app/www/tmp
RUN chown app:app /home/app/www/public/system
RUN chown app:app /home/app/www/log/production.log


RUN mkdir -p /etc/my_init.d
ADD config/docker/startup.sh /etc/my_init.d/startup.sh

RUN chmod +x /etc/my_init.d/startup.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*