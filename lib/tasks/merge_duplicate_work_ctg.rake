desc 'update work_category_id in issues'
task merge_duplicate_work_ctg: :environment do
  WorkCategory.all.each do |ctg|
    ctg.issues.where('work_category_id = ?', 1).update_all('work_category_id': 38)
    ctg.issues.where('work_category_id = ?', 2).update_all('work_category_id': 39)
    ctg.issues.where('work_category_id = ?', 3).update_all('work_category_id': 40)
    ctg.issues.where('work_category_id = ?', 4).update_all('work_category_id': 41)
    ctg.issues.where('work_category_id = ?', 5).update_all('work_category_id': 42)
    ctg.issues.where('work_category_id = ?', 6).update_all('work_category_id': 43)
    ctg.issues.where('work_category_id = ?', 7).update_all('work_category_id': 44)
    ctg.issues.where('work_category_id = ?', 8).update_all('work_category_id': 45)
    ctg.issues.where('work_category_id = ?', 9).update_all('work_category_id': 46)
    ctg.issues.where('work_category_id = ?', 10).update_all('work_category_id': 47)
    ctg.issues.where('work_category_id = ?', 11).update_all('work_category_id': 48)
    ctg.issues.where('work_category_id = ?', 12).update_all('work_category_id': 49)
    ctg.issues.where('work_category_id = ?', 13).update_all('work_category_id': 50)
    ctg.issues.where('work_category_id = ?', 14).update_all('work_category_id': 51)
    ctg.issues.where('work_category_id = ?', 15).update_all('work_category_id': 52)
    ctg.issues.where('work_category_id = ?', 16).update_all('work_category_id': 53)
    ctg.issues.where('work_category_id = ?', 17).update_all('work_category_id': 54)
    ctg.issues.where('work_category_id = ?', 18).update_all('work_category_id': 55)
    ctg.issues.where('work_category_id = ?', 19).update_all('work_category_id': 56)
    ctg.issues.where('work_category_id = ?', 20).update_all('work_category_id': 57)
    ctg.issues.where('work_category_id = ?', 21).update_all('work_category_id': 58)
    ctg.issues.where('work_category_id = ?', 22).update_all('work_category_id': 59)
    ctg.issues.where('work_category_id = ?', 23).update_all('work_category_id': 60)
    ctg.issues.where('work_category_id = ?', 24).update_all('work_category_id': 61)
    ctg.issues.where('work_category_id = ?', 25).update_all('work_category_id': 62)
    ctg.issues.where('work_category_id = ?', 26).update_all('work_category_id': 63)
    ctg.issues.where('work_category_id = ?', 27).update_all('work_category_id': 64)
    ctg.issues.where('work_category_id = ?', 28).update_all('work_category_id': 65)
    ctg.issues.where('work_category_id = ?', 29).update_all('work_category_id': 66)
    ctg.issues.where('work_category_id = ?', 30).update_all('work_category_id': 67)
    ctg.issues.where('work_category_id = ?', 31).update_all('work_category_id': 68)
    ctg.issues.where('work_category_id = ?', 32).update_all('work_category_id': 69)
    ctg.issues.where('work_category_id = ?', 33).update_all('work_category_id': 70)
    ctg.issues.where('work_category_id = ?', 34).update_all('work_category_id': 71)
    ctg.issues.where('work_category_id = ?', 35).update_all('work_category_id': 72)
    ctg.issues.where('work_category_id = ?', 36).update_all('work_category_id': 73)
    ctg.issues.where('work_category_id = ?', 37).update_all('work_category_id': 74)
  end
  ctg = WorkCategory.find(73)
  ctg.service_id = 7
  ctg.save!
end