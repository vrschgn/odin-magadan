desc 'delete duplicate work categories'
task delete_duplicate_work_ctg: :environment do
  WorkCategory.where(id: 1..37).destroy_all
end