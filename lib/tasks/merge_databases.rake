
desc 'Rewriting data from mysql to psql'
task merge_databases: :environment do
	old_issues = Mysql::Ticket.all.where.not(status: [8, 2, 6, 7, 4], description: "ППР").where(ticket_type: 1)
	old_issues.each do |old_issue|
		p old_issue
		status = case old_issue["status"]
							when 0 then 'initial'
							when 1 then 'accepted'
							when 3 then 'processing'
						end
		issues_comments = "" 
		old_issue.change.each { |c| issues_comments += " Пользователь: '#{c.user_name}'; Создано: '#{c.created_at}'; Комментарий: '#{c.comment}'. " }
		
		old_company_name = old_issue.try(:company).try(:name).to_s 
		new_company = Company.where("name LIKE ?", "%#{old_company_name}%").first

		new_issue = MaintenanceIssue.new(tel: old_issue["phone"],
													dead_line: old_issue["pref_time_start"],
													description: old_company_name + " (" + old_issue["building_name"] + ") " + old_issue["description"] + issues_comments, 
													created_at: old_issue["created_at"], 
													updated_at: old_issue["updated_at"],
													user_id: 2,
													company_id: new_company ? new_company.id : 1,
													state: status,
													executor_id: old_issue["executor_id"],
													service_id: 6,
													responsible_full_name: old_issue["contact_person"],
													is_commercial: false)
		new_issue.save!
		puts new_issue.attributes
		puts new_issue.errors.messages
	end
end


